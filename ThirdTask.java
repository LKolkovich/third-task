import java.util.Scanner;

public class ThirdTask {
    public static void main(String[] args) {
        int value = 1;
        int previousValue = 0;
        int container = 0;
        for(int i = 0; i < 11; i++){
            System.out.print(value + " ");
            container = value;
            value = value + previousValue;
            previousValue = container;
        }
        System.out.println();

        int i = 0;
        value = 1;
        previousValue = 0;
        while(i < 11){
            System.out.print(value + " ");
            container = value;
            value = value + previousValue;
            previousValue = container;
            i++;
        }

    }
}
